package com.app.handler;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;

import com.fasterxml.jackson.databind.ObjectMapper;



public class CustomWIH implements WorkItemHandler{

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager)
	{
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager)
	{
			
			Object param1 =  workItem.getParameter("CandidateOut");
			
			System.out.println(param1);
			Object jsonObject=null;
			// final String uri = "http://localhost:9899/candidate/register";
		     try{
			  ObjectMapper objectMapper= new ObjectMapper();
			  jsonObject= objectMapper.writeValueAsString(param1);
			  System.out.println("the json object is this::: "+jsonObject);
		     }catch(Exception e){
		    	 System.out.println(e.getMessage());
		     }
		     System.out.println("Update"); 
		    Map<String, Object> resultMap=new HashMap<String, Object>();
		    resultMap.put("Result", jsonObject);
		    manager.completeWorkItem(workItem.getId(), resultMap);
		    
		    
			    
			  /*  try{
			    RestTemplate restTemplate = new RestTemplate();
			    String result = restTemplate.postForObject(uri, param1,String.class);
			    System.out.println(result);
			    }catch(Exception e){
			    	e.printStackTrace();
			    	System.out.println(e.getMessage());
			    }
			   */
		
		 
	}
}
